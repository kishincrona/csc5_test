/* 
 * File:   main.cpp
 * Author: awesomesauce man
 *
 * Created on February 20, 2014, 10:33 AM
 */

//#include <cstdlib> //cstdlib library
#include <iostream> //iostream is needed for output

// Gives the context of where my libraries are coming from
using namespace std;

/*
 * 
 */
// There is always and only one main
// Programs always start at main
// Programs execute from top to bottom, left to right
int main(int argc, char** argv) {
    // cout specifies output
    // endl creates a new line
    // << specifies stream operator
    // all statements end in a semicolon
    
    // using a program-defined identifier/variable
    // message is a variable 
    // string is a data type
    // = is a data type
    // assign right to left
    // string message = "hello world";
    string message; // variable declaration
    //message = "hello world"; // variable initialization
    cout << "please enter a message";
    cin >> message;
    cout << "your message is " << message << endl;
    
    // c++ ignored whitespace
    cout
    <<
            "Dmitri Charbonneau";
    
    // if program hits return, it ran successfully
    return 0;
} // All my code is within curly braces

