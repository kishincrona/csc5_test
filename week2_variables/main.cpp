/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2014, 11:29 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int num_1,num_2;
    
    //output junk values
    cout << num_1 << " " << num_2 << endl;
    
    //initialization
    num_1 = num_2 = 0;
    
    cout << num_1 << " " << num_2 << endl;
    
    //get user input for both values
    cout << "please enter two integers" << endl;
    
    cin >> num_1 >> num_2;
    
    cout << "you entered: " << endl
        << num_1 << " " << num_2 << endl; 
    
    // calculate average of two numbers
    int total = num_1 + num_2;
    
    cout << endl << "total: " << total << endl;
    
    // case of integer division
    double averageintdivision = total / 2;
    
    double averagestaticcast = static_cast <double> (total) /2
    double averagedecimal = total /2.0;
    
    cout << "averageintdivision: " << averageintdivision << endl; 
    cout << "averagedecimal: " <<averagedecimal << endl;
    //output average
    
    return 0;
}

